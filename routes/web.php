<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/shop', 'MainController@index')->name('shop');
Route::post('/Excel','MainController@excel')->name('excel');
Route::post('/CreateExcel','MainController@CreateExcel')->name('CreateExcel');
Route::post('/CrearTarifa','MainController@uniqueTar')->name('CrearTarifa');
Route::get('/SyncProd','MainController@SyncProduct')->name('SyncProd');
Route::post('/alterProduct','MainController@alterProduct')->name('alterProduct');
Route::post('/currier','MainController@createCurrier')->name('currier');
//Route::get('/install','MainController@installTar')->name('install');
Route::get('/', 'ShopifyController@index')->middleware(['auth.shop', 'billable'])->name('home');
Route::post('/response', 'WebHookController@CarrierService')->name('response');
Route::post('/app-uninstalled','WebHookController@UninstallApp');
Route::post('/order-created','WebHookController@OrderCreate');