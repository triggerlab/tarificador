<?php

// Set variables for our request
$shop = $_GET['shop'];

$api_key = "86d1d0f31d2c227746bb691180380120";
$scopes = "read_shipping, write_shipping";
$redirect_uri = "https://triggerlab.cl/laravel/public/";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();
?>