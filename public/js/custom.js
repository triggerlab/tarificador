
const modal = document.querySelector("#my-modal");
const modalBtn = document.querySelector("#btnmodalXls");
const closeBtn = document.querySelector("#closeXls");

// Events
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

// Open
function openModal() {
  modal.style.display = 'block';
}

// Close
function closeModal() {
  modal.style.display = 'none';
}

// Close If Outside Click
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = 'none';
  }
}
function thisFileUpload() {
    document.getElementById("file").click();
};
const modal1 = document.querySelector("#my-modal1");
const modalBtn1 = document.querySelector("#btnmodalXls1");
const closeBtn1 = document.querySelector("#closeXls1");

// Events
modalBtn1.addEventListener('click', openModal1);
closeBtn1.addEventListener('click', closeModal1);
window.addEventListener('click', outsideClick);

// Open
function openModal1() {
  modal1.style.display = 'block';
}

// Close
function closeModal1() {
  modal1.style.display = 'none';
}


const modal2 = document.querySelector("#my-modal2");
const modalBtn2 = document.querySelector("#btnmodalXls2");
const closeBtn2 = document.querySelector("#closeXls2");

// Events
modalBtn2.addEventListener('click', openModal2);
closeBtn2.addEventListener('click', closeModal2);
window.addEventListener('click', outsideClick);

// Open
function openModal2() {
  modal2.style.display = 'block';
}

// Close
function closeModal2() {
  modal2.style.display = 'none';
}


const modal3 = document.querySelector("#my-modal3");
const modalBtn3 = document.querySelector("#btnmodalXls3");
const closeBtn3 = document.querySelector("#closeXls3");

// Events
modalBtn3.addEventListener('click', openModal3);
closeBtn3.addEventListener('click', closeModal3);
window.addEventListener('click', outsideClick);

// Open
function openModal3() {
  modal3.style.display = 'block';
}

// Close
function closeModal3() {
  modal3.style.display = 'none';
}
