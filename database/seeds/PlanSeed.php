<?php

use Illuminate\Database\Seeder;
use DB;

class PlanSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'type' => 1,
            'name' => 'unique plan',
            'price' => 25.00,
            'capped_amount' => 0.00,
            'terms' => 'Test terms',
            'trial_days' => 0,
            'test' => TRUE,
            'on_install' => 1,
            'created_at' => NULL,
            'updated_at' => NULL
        ]);
    }
}
