<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id_pro');
            $table->string('nombre_producto');
            $table->float('weight');
            $table->float('height');
            $table->float('width');
            $table->float('lenght');
            $table->string('id_prod_shop')->unique();
            $table->unsignedBigInteger('id_tienda');
            $table->foreign('id_tienda')->references('id_tienda')->on('tienda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
