<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnvioGratisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envio_gratis', function (Blueprint $table) {
            $table->bigIncrements('id_envio');
            $table->string('condicion');
            $table->string('valor');
            $table->unsignedBigInteger('id_tienda');
            $table->foreign('id_tienda')->references('id_tienda')->on('tienda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envio_gratis');
    }
}
