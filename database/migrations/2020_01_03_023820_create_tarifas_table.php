<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarifasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifa', function (Blueprint $table) {
            $table->bigIncrements('id_tar');
            $table->string('nombre_tarifa');
            $table->string('pais');
            $table->string('region');
            $table->string('comuna');
            $table->string('moneda');
            $table->integer('precio_base');
            $table->integer('precio_x_kg')->nullable();
            $table->integer('tiempo_min')->nullable();
            $table->integer('tiempo_max')->nullable();
            $table->float('desde')->nullable();
            $table->unsignedBigInteger('id_tienda');
            $table->foreign('id_tienda')->references('id_tienda')->on('tienda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifa');
    }
}
