<div class="Polaris-Card__Section">
    <div class="Polaris-EmptyState Polaris-EmptyState--withinContentContainer">
        <div class="Polaris-EmptyState__Section">
        <div class="Polaris-EmptyState__DetailsContainer">
            <div class="Polaris-EmptyState__Details">
            <div class="Polaris-TextContainer">
                <p class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Sube tu archivo para comenzar</p>
                <div class="Polaris-EmptyState__Content">
                <p>Se permiten extensiones xls, xlsx, csv</p>
                </div>
            </div>
            <div class="Polaris-EmptyState__Actions">
                <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                <div class="Polaris-Stack__Item"><button type="file" class="Polaris-Button Polaris-Button--primary" onclick="thisFileUpload();"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Upload files</span></span></button></div>
                </div>
            </div>
            </div>
        </div>
        <div class="Polaris-EmptyState__ImageContainer"><img src="https://cdn.shopify.com/s/files/1/2376/3301/products/file-upload-empty-state.png" role="presentation" alt="" class="Polaris-EmptyState__Image"></div>
        </div>
    </div>
</div>