<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <link
  rel="stylesheet"
  href="https://unpkg.com/@shopify/polaris@4.10.2/styles.min.css"
/>
<link rel="stylesheet" href="bootstrap-polaris.min.css" />
<link rel="stylesheet" href="{{asset('css/custom.css')}}"/>



</head>

<body>
<form id="formXls">
    @csrf
    <input type="file" id="file" name="file" style="display:none;" />
</form>

  <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
    <div class="Polaris-Card">
      <div class="Polaris-Card__Section">
        <div class="Polaris-SettingAction">
          <div class="Polaris-SettingAction__Setting">
            <div class="Polaris-Stack">
              <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                <div class="Polaris-AccountConnection__Content">
                  <!--<div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Ayuda?</span></span></button>-->
                  
                  </div>
                </div>
                
              </div>
              
            </div>
          </div>
          <div class="Polaris-SettingAction__Action"><a class="Polaris-Button Polaris-Button--primary" href="{{asset('/tipo.xlsx')}}" download><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Descargar Excel tipo</span></span></a></div>
          <div class="Polaris-SettingAction__Action"><button type="button" id="btnmodalXls" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Subir XLS</span></span></button></div>
          <div class="Polaris-SettingAction__Action"><button type="button" id="btnmodalXls1" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Nueva Tarifa</span></span></button></div>
          <div class="Polaris-SettingAction__Action"><button type="button" id="btnmodalXls2" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Productos</span></span></button></div>
          <div class="Polaris-SettingAction__Action"><button type="button" id="btnmodalXls3" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Curriers</span></span></button></div>
          <div id="my-modal" class="modal">
          <div class="modal-content">
            <div class="modal-header">
              <div class="Polaris-Stack" style="width:100%">
                <div class="Polaris-Stack">
                  <h2 class="Polaris-Stack__Item">Subir Xls</h2>
                </div>
                <div class="Polaris-Stack" style="margin-left:auto; margin-right:0;">
                  <div style="display:none" id="btnmodalXlsDiv" class="Polaris-Stack__Item"><button onclick="thisFileUpload();" type="button" id="btnmodalXlsDiv" class="Polaris-Button Polaris-Button--primary">Subir otro XLS</button></div>
                  <span class="close Polaris-Stack__Item" id="closeXls">&times;</span>
                </div>
              </div>
            </div>
            <div class="modal-body" id="modalBodyXls">
                  @include('fileInput')
                  <!--fin card-->  
            </div>
            <div class="modal-footer">
            </div>
          </div>
        </div>
        <div id="my-modal1" class="modal">
          <div class="modal-content">
            <div class="modal-header">
              <div class="Polaris-Stack" style="width:100%">
                <div class="Polaris-Stack">
                  <h2 class="Polaris-Stack__Item">Nueva Tarifa Personalizada</h2>
                </div>
                <div class="Polaris-Stack" style="margin-left:auto; margin-right:0;">
                  <span class="close Polaris-Stack__Item" id="closeXls1">&times;</span>
                </div>
              </div>
            </div>
            <div class="modal-body" id="modalBodyXls">
              <form method="POST" id="tarifa_form">
                @csrf
                <div class="row">
                  <label for="nombre_tarifa">Nombre Tarifa</label>
                  <input type="text" name="nombre_tarifa" required>
                  <label for="pais">Region</label>
                  <select name="pais" id="pais" required>
                  </select>
                  <label for="comuna">comuna</label>
                  <select name="comuna" id="comuna" required>
                  </select>
                  <label for="moneda">moneda</label>
                  <input type="text" name="moneda" required>
                </div>
                <div class="row">
                  <label for="precio_base">Precio Base</label>
                  <input type="text" name="precio_base" required>
                  <label for="precio_kg">Precio x kg</label>
                  <input type="text" name="precio_kg">
                  <label for="tiempo_min">Tiempo minimo</label>
                  <input type="text" name="tiempo_min">
                  <label for="tiempo_max">tiempo maximo</label>
                  <input type="text" name="tiempo_max">
                </div>
                <div class="row">
                  <label for="desde">Desde (precio x kg)</label>
                  <input type="text" name="desde">
                </div>
                <input type="submit" value="crear tarifa">
              </form>
            </div>
            <div class="modal-footer">
            </div>
          </div>
        </div>
         
        </div>
        <div class="Polaris-AccountConnection__TermsOfService">
        </div>
      </div>
    </div>
  </div>
  <div id="my-modal2" class="modal">
    <div class="modal-content">
      <div class="modal-header">
        <div class="Polaris-Stack" style="width:100%">
          <div class="Polaris-Stack">
            <h2 class="Polaris-Stack__Item">Productos</h2>
          </div>
          <div class="Polaris-Stack" style="margin-left:auto; margin-right:0;">
            <div id="btnmodalXlsDiv" class="Polaris-Stack__Item"><button type="button" id="productoSync" class="Polaris-Button Polaris-Button--primary">syncronzar productos</button></div>
            <span class="close Polaris-Stack__Item" id="closeXls2">&times;</span>
          </div>
        </div>
      </div>
      <div class="modal-body" id="modalBodyXls">
      <table style="width:100%">
        <tr>
          <th align="justify">producto</th>
          <th align="justify">peso</th>
          <th align="justify">alto</th>
          <th align="justify">ancho</th>
          <th align="justify">profundidad</th>
          <th align="justify"></th>
        </tr>
        @if($producto != null)
            @foreach($producto as $key => $p)
              <tr>
                <!--{{$key}}-->
                  <td>{{$p->nombre_producto}}</td>
                  <form id="ProductForm{{$p->id_pro}}">
                    @csrf
                    <input type="hidden" value="{{$p->id_pro}}" name="id_prod">
                    <td><input type="text" value="{{$p->weight}}" name="peso" required></td>
                    <td><input type="text" value="{{$p->height}}" name="alto" required></td>
                    <td><input type="text" value="{{$p->width}}" name="ancho" required></td>
                    <td><input type="text" value="{{$p->lenght}}" name="profundidad" required></td>
                    <td><button type="submit" id="btnmodalXls" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Guardar</span></span></button></td>
                  </form>
              </tr>
              <script>
              document.getElementById('ProductForm{{$p->id_pro}}').addEventListener('submit',function(e){
                e.preventDefault()
                //alert('se mando')
                var formdata = new FormData(document.getElementById('ProductForm{{$p->id_pro}}'));
                $.ajax({
                  url:"{{route('alterProduct')}}",
                  type: "post",
                  dataType: "html",
                  data: formdata,
                  cache: false,
                  contentType: false,
                  processData: false,
                  success:function(e){
                    //alert('se envio')
                  }
                })
                //console.log(formdata);
              })
              </script>
            @endforeach
        @endif
      </table> 
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
  <div id="my-modal3" class="modal">
    <div class="modal-content">
      <div class="modal-header">
        <div class="Polaris-Stack" style="width:100%">
          <div class="Polaris-Stack">
            <h2 class="Polaris-Stack__Item">Ingresa los curriers</h2>
          </div>
          <div class="Polaris-Stack" style="margin-left:auto; margin-right:0;">
            <div style="display:none" id="btnmodalXlsDiv" class="Polaris-Stack__Item"><button onclick="thisFileUpload();" type="button" id="btnmodalXlsDiv" class="Polaris-Button Polaris-Button--primary">Subir otro XLS</button></div>
            <span class="close Polaris-Stack__Item" id="closeXls3">&times;</span>
          </div>
        </div>
      </div>
      <div class="modal-body" id="modalBodyXls">
      <table style="width:100%">
        <tr>
          <th align="justify">Currier</th>
          <th align="justify">api_key</th>
          <th align="justify"></th>
        </tr>
        @foreach($curriers as $currier)
        </tr>
          <td>{{$currier->nombre}}</td>
          @if($currier->nombre != 'Chilexpress' && $currier->nombre != 'RocketCurrier')
            <td>Proximamente</td>
          @endif
          @if($currier->nombre == 'Chilexpress')
          <form action="" id="{{$currier->nombre}}Form">
            @csrf
            <td><input type="text" name="api_key" value="{{$currier->api_key}}" required><input type="hidden" name="currier" value="{{$currier->nombre}}"></td>
            <td><button type="submit" id="btnmodalXls" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Guardar</span></span></button></td>
          </form>
          @endif
          @if($currier->nombre == 'RocketCurrier')
              <td><input type="checkbox" id="rocket" name="rocket" value="rocket"></td>
          @endif
        </tr>
        @endforeach
      </table> 
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
  <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
    <div class="Polaris-Page">
      <div class="Polaris-Page-Header">
        <div class="Polaris-Page-Header__MainContent">
          <div class="Polaris-Page-Header__TitleActionMenuWrapper">
            <div>
              <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                <div class="Polaris-Header-Title">
                  <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">Lista del XLS</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Page__Content">
        <div class="Polaris-Card">
          <div class="">
            <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                        <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
                      </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                        <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
                      </svg></span></span></span></button></div>
            <div class="Polaris-DataTable">
              <div class="Polaris-DataTable__ScrollContainer">
                <table class="Polaris-DataTable__Table">
                  <thead>
                    <tr>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header" scope="col">Tarifa</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">destino</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">precio base - moneda</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">precio por Kg - desde</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">timepo min - max</th>
                    </tr>
                  </thead>
                  @if($tarifas != null)
                  <tbody>
                    @foreach($tarifas as $col)
                    <tr class="Polaris-DataTable__TableRow">
                      <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{$col->nombre_tarifa}}</th>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col->pais}}-{{$col->region}}-{{$col->comuna}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col->precio_base}} - {{$col->moneda}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col->precio_x_kg}} - {{$col->desde}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col->tiempo_min}} - {{$col->tiempo_max}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--total Polaris-DataTable--cellTotalFooter" scope="row">Total de destinos</th>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable--cellTotalFooter"></td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable--cellTotalFooter"></td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable--cellTotalFooter Polaris-DataTable__Cell--numeric">#{{$tarifas->count()}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable--cellTotalFooter Polaris-DataTable__Cell--numeric"></td>
                    </tr>
                  </tfoot>
                  @endif
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script>
  fetch("{{asset('js/region.json')}}")
  .then(response => response.json())
  .then(function(json){
    const array = json.regiones
    array.map(function(x){
      select = document.getElementById('pais');
      select.options[select.options.length] = new Option(x.region, x.region)
    })
  });
  document.getElementById('pais').addEventListener('change',function(x){
    alert(document.getElementById('pais').value);
    fetch("{{asset('js/region.json')}}")
      .then(response => response.json())
      .then(function(json){
        const array = json.regiones
        const Farray = array.find(x => x.region === document.getElementById('pais').value)
        select = document.getElementById('comuna');
        select.innerHTML = ""
        Farray.comunas.map(function(x){
          select.options[select.options.length] = new Option(x, x)
        })
      });
  }) 
  document.getElementById('tarifa_form').addEventListener('submit',function(e){
    e.preventDefault();
    formData = new FormData(document.getElementById('tarifa_form'))
    $.ajax({
      url:"{{route('CrearTarifa')}}",
      type: "post",
      dataType: "html",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success:function(e){
        alert('se envio')
      }
    })
  })
</script>
<script>
document.getElementById('file').addEventListener('change',function(e){
    formData = new FormData(document.getElementById('formXls'));
    $.ajax({
          url: "{{route('excel')}}",
          type: "post",
          dataType: "html",
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function() {  
              document.getElementById('modalBodyXls').innerHTML = "<div style='margin: 0 auto;'><img src='https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif'/></div>";
          },
          success:function(e){
              document.getElementById('modalBodyXls').innerHTML = e;
              document.getElementById('btnmodalXlsDiv').style.display = 'flex';
              document.getElementById('subirExcel').addEventListener('click',function(){
                          formData = new FormData(document.getElementById('formXls'));
                      $.ajax({
                              url: "{{route('CreateExcel')}}",
                              type: "post",
                              dataType: "html",
                              data: formData,
                              cache: false,
                              contentType: false,
                              processData: false,
                              beforeSend: function() {  
                                  document.getElementById('modalBodyXls').innerHTML = "<div style='margin: 0 auto;'><img src='https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif'/></div>";
                              },
                              success:function(e){
                                  document.getElementById('modalBodyXls').innerHTML = "<div style='margin: 0 auto;'><img src='https://media1.tenor.com/images/5ca3f2f8ebe9fbd08d0f9b7afb99b408/tenor.gif?itemid=14249951' Loop=False/></div>";
                                  document.getElementById('btnmodalXlsDiv').style.display = 'flex';
                              }
                          })
                      });
          }
      })
  });
</script>
<script>
document.getElementById('productoSync').addEventListener('click',function(e){
  $.ajax({
      url:"{{route('SyncProd')}}",
      type: "get",
      dataType: "html",
      cache: false,
      contentType: false,
      processData: false,
      success:function(e){
        alert('se envio')
      }
    })
})
</script>
<script>
document.getElementById('ChilexpressForm').addEventListener('submit',function(e){
  e.preventDefault()
  formData = new FormData(document.getElementById('ChilexpressForm'));
  $.ajax({
    url:"{{route('currier')}}",
    type:"POST",
    dataType: "html",
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
          success:function(e){
            alert('se envio')
          }
  })
})
</script>
<script>
document.getElementById('')
</script>

</html>
