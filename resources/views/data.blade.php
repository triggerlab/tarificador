
<div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
    <div class="Polaris-DataTable" >
        <div class="Polaris-DataTable__ScrollContainer" style="height:500px">
            <table class="Polaris-DataTable__Table">
            <thead>
                <tr>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header" scope="col">Tarifa</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">destino</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">precio base - moneda</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">precio por Kg - desde</th>
                      <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">timepo min - max</th>
                </tr>
                <!--<tr>
                <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--total" scope="row">Totals</th>
                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total"></td>
                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total"></td>
                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable__Cell--numeric">255</td>
                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--total Polaris-DataTable__Cell--numeric">$155,830.00</td>
                </tr>-->
            </thead>
            <tbody>
                    @foreach($collection as $col)
                            @if(isset($col['error']))
                            <tr class="Polaris-DataTable__TableRow" style="background:red;">
                                <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{$col['nombre_tarifa']}}</th>
                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col['error']}}</td>
                            </tr>
                            @else
                            <tr class="Polaris-DataTable__TableRow">
                      <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{$col['nombre_tarifa']}}</th>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col['pais']}}-{{$col['region']}}-{{$col['comuna']}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col['precio_base']}} - {{$col['moneda']}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col['precio_x_kg']}} - {{$col['desde']}}</td>
                      <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{$col['tiempo_min']}} - {{$col['tiempo_max']}}</td>
                            </tr>
                            @endif
                    @endforeach
            </tbody>
            </table>
        </div>
        </div>
            <?php
                $collection = $collection->where('error','!=',null)
            ?>
            @if($collection->count() != 0)
                <div id="containerButon" style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;text-align: center;"><button style="background:#c0392b;color:white" onclick="alert('Su excel contiene {{$collection->count()}} errores');" type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Crear Tarifas</span></span></button></div>
            @else
                <div id="containerButon" style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;text-align: center;"><button id="subirExcel" style="background:#2ecc71;color:white" type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Crear Tarifas</span></span></button></div>
                
            @endif
        </div>

        
