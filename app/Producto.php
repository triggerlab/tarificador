<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Producto extends Model
{
    protected $table = 'productos';

    protected $primaryKey = 'id_pro';

    protected $fillable =['nombre_producto','weight','height','width','lenght','id_prod_shop','id_tienda'];

    public static function ProductoPorNombreTienda($id){
        return DB::table('productos')
        ->select('*')
        ->where('id_tienda','=',$id)
        ->get();
    }
    public static function GetProductsToCurrier($id,$idProd){
        return DB::table('productos')
        ->select('*')
        ->where('id_prod_shop','=',$idProd)
        ->where('id_tienda','=',$id)
        ->first();
    }
}
