<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Str;

class Tarifa extends Model
{
    protected $table = 'tarifa';

    protected $primaryKey = 'id_tar';

    protected $fillable =['nombre_tarifa','pais','region','comuna','precio_base','moneda','precio_x_kg','desde','tiempo_min','tiempo_max','id_tienda'];

    public static function TarifaporNombreTienda($id){
        return DB::table('tarifa')
        ->select('*')
        ->where('id_tienda','=',$id)
        ->get();
    }
    
}
/*
$table->bigIncrements('id_tar');
            $table->string('nombre_tarifa');
            $table->string('pais')->nulleable();
            $table->string('region')->nulleable();
            $table->string('comuna')->nulleable();
            $table->string('marca')->nulleable();
            $table->integer('descuento_marca')->nulleable();
            $table->integer('precio_base')->nulleable(0);
            $table->integer('precio_x_kg')->nulleable(0);
            $table->float('desde')->nulleable();
            $table->unsignedBigInteger('id_tienda');
            $table->foreign('id_tienda')->references('id_tienda')->on('tienda');
            $table->timestamps();
*/