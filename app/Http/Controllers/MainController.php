<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ExcelImport;
use App\Imports\TarifaImport;
use App\Tienda;
use App\Tarifa;
use App\Producto;
use App\currier;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use ShopifyApp;
use Request as Req;

class MainController extends Controller
{
    public function index(Request $request){
        $shop = ShopifyApp::shop();
        if(Tarifa::all()->count() != 0){
            $tarifas = Tarifa::TarifaporNombreTienda(Tienda::porNombreTienda($shop->shopify_domain)->id_tienda);
        }else{
            $tarifas = 0;
        }
        if(Producto::all()->count() != 0){
            $producto = Producto::ProductoPorNombreTienda(Tienda::porNombreTienda($shop->shopify_domain)->id_tienda);
        }else{
            $producto = 0;
        }
        
        $curriers = currier::where('id_tienda','=',Tienda::porNombreTienda($shop->shopify_domain)->id_tienda)->get();
        return view('welcome',compact('tarifas','producto','curriers'));
        //return $producto;
        //return $curriers;
    }
    public function uniqueTar(Request $request){
        $shop = ShopifyApp::shop();
        Tarifa::create([
            'nombre_tarifa' => $request->nombre_tarifa,
            'pais' => 'Chile',
            'region' => $request->pais,
            'comuna' => $request->comuna,
            'moneda' => $request->moneda,
            'precio_base' => $request->precio_base,
            'precio_x_kg' => $request->precio_kg,
            'tiempo_min' =>$request->tiempo_min,
            'tiempo_max' => $request->tiempo_max,
            'desde' => $request->desde,
            'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
        ]);
        return response()->json(['success' => 'success'], 200);
    }
    public function excel(Request $request){
        $excel = new ExcelImport;
        $collection = ($excel)->toCollection($request->file);
        $collection->map(function ($a){
            $a->map(function ($b){
                $validator = Validator::make($b->toArray(),[
                    'nombre_tarifa' => 'required',
                ]);
                if ($validator->fails()){
                    $b['error'] = $validator->messages()->first();
                }
            });
        });
        $collection = $collection[0];
        $collection = collect($collection);
        $collection = $collection->sortByDesc('error');
        return view('data',compact('collection'));
    }
    public function CreateExcel(Request $request){
        if(Excel::import(new TarifaImport,$request->file)){
            return 'funciono';
        }else{
            return 'fail';
        }

    }
    public function SyncProduct(){
        $shop = ShopifyApp::shop();
        $products = $shop->api()->rest('GET','/admin/api/2020-01/products.json');
        $json = [];
        foreach(collect($products->body->products) as $prod){
            array_push($json,array(
                'nombre' => $prod->title,
                'wigth' => $prod->variants[0]->weight
            ));
            if(Producto::where('nombre_producto','=',$prod->title)->count() == 0){
                Producto::create([
                    'nombre_producto' => $prod->title,
                    'weight' => $prod->variants[0]->weight/1000,
                    'height' => 0.00,
                    'width' => 0.00,
                    'lenght' => 0.00,
                    'id_prod_shop' => $prod->id,
                    'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
                ]);
            }
        };
        
        return $json;
    }
    public function alterProduct(Request $request){
        $prod = Producto::find($request->id_prod);
        $prod->update([
            'weight' => $request->peso,
            'height' => $request->alto,
            'width' => $request->ancho,
            'lenght' => $request->profundidad
        ]);
    }
    public function createCurrier(Request $request){
        $shop = ShopifyApp::shop();
        $currier = currier::where('nombre','=',$request->currier)->where('id_tienda','=',Tienda::porNombreTienda($shop->shopify_domain)->id_tienda)->first();
        $currier = currier::find($currier->id_currier);
        $currier->update([
            'api_key' => $request->api_key
        ]);
        return $currier;
    }
}
