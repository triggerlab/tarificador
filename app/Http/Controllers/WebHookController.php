<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarifa;
use App\Tienda;
use DB;
use App\Shop;
use App\currier;
use App\Producto;
use GuzzleHttp\Psr7\Request as Guzz;
use GuzzleHttp\Client;
use ShopifyApp;

class WebHookController extends Controller
{
    public function UninstallApp(Request $request){
        $tiendax = $request->header('X-Shopify-Shop-Domain');
		$tienda = DB::table('shops')->get();
		$tienda = $tienda->where('shopify_domain','=',$tiendax)->first();
		DB::table('shops')->delete($tienda->id);
	}
    public function CarrierService(Request $request){
        $tienda = $request->header('X-Shopify-Shop-Domain');
        $tarifas = Tarifa::TarifaporNombreTienda(Tienda::porNombreTienda($tienda)->id_tienda);
        $jsonS = [];
        $date = date("Y-m-d H:i:s");
        $contador = 0;
        $weight = 0;
        $height = 0;
        $width = 0;
        $lenght=0;
        $declarated=0;
        foreach(json_decode($request->getContent(),true)['rate']['items'] as $item){
            $producto = Producto::GetProductsToCurrier(Tienda::porNombreTienda($tienda)->id_tienda,$item['product_id']);
            if($producto->weight != 0.00 && $producto->height != 0.00 && $producto->width != 0.00 && $producto->lenght != 0.00){
                $contador++;
                $weight = $weight + $producto->weight;
                $height = $height + $producto->height;
                $width = $width + $producto->width;
                $lenght = $lenght + $producto->lenght;
                $declarated = $declarated + $item['price'];
            }
        }
        foreach($tarifas as $tarifa){
            if(trim(strtolower($tarifa->comuna)) == trim(strtolower(json_decode($request->getContent(),true)['rate']['destination']['city']))){
                $datemin = '';
                $datemax = '';
                if($tarifa->tiempo_min != null){
                    $datemin = strval($tarifa->tiempo_min);
                }else{
                    $datemin = '30';
                }
                if($tarifa->tiempo_max != null){
                    $datemax = strval($tarifa->tiempo_max);
                }else{
                    $datemax = '60';
                }
                array_push($jsonS,array(
                    "service_name"=> $tarifa->nombre_tarifa,
                    "service_code" => $tarifa->id_tar,
                    "total_price" => $tarifa->precio_base."00",
                    "currency" => $tarifa->moneda,
                    "description" => 'Esta es una tarifa de la tienda :D',//collect(json_decode($request->getContent(),true)['rate']['items'])->count(),
                    "min_delivery_date" => date('Y-m-d H:i:s', strtotime($date. ' + '.$datemin.' days')),
                    "max_delivery_date" => date('Y-m-d H:i:s', strtotime($date. ' + '.$datemax.' days'))
                ));
            }
                if(collect(json_decode($request->getContent(),true)['rate']['items'])->count() == $contador){
                    //aqui se ejecutan los metodos de cada uno de los curriers
                    $Chilexpresss = currier::where('nombre','=','Chilexpress')->where('id_tienda','=',Tienda::porNombreTienda($tienda)->id_tienda)->first();
                    if($Chilexpresss->api_key != '' ){
                        array_push($jsonS,$this->Chilexpresss(json_decode($request->getContent(),true)['rate'],$weight,$height,$width,$lenght,$declarated,strtoupper(json_decode($request->getContent(),true)['rate']['origin']['city']),strtoupper(json_decode($request->getContent(),true)['rate']['destination']['city']),$Chilexpresss->api_key));
                    }
                }
        }
        $json = array(
            'rates'=>array(
                /*array(
                    "service_name"=> json_decode($request->getContent(),true)['rate']['destination']['city'],
                    "service_code" => "ON",
                    "total_price" => "32",
                    "currency" => "USD",
                    "description" => "djkslfjdklsfjkl",
                    "min_delivery_date" => "2020-04-12 14:48:45 -0400",
                    "max_delivery_date" => "2020-04-20 14:48:45 -0400"
                ),
                array(
                    'service_name' => 'Endertech Regular',
                    'service_code' => 'ETREG',
                    'total_price' => '80',
                    'currency' => 'USD',
                    'min_delivery_date' => "2020-04-12 14:48:45 -0400",
                    'max_delivery_date' => "2020-04-16 14:48:45 -0400"
                )*/
                $jsonS
                )
        );
        return $json;
    }
    public function Chilexpresss($items,$weight,$height,$width,$lenght,$declarated,$from,$destination,$api_key){
        $comuna = [
			'ACHA' => 'ACHAO',
			'AHOS' => 'ALTO HOSPICIO',
			'ALAM' => 'LOS ALAMOS',
			'ANCU' => 'ANCUD',
			'ANDA' => 'ANDACOLLO',
			'ANGO' => 'ANGOL',
			'ANTO' => 'ANTOFAGASTA',
			'ARAU' => 'ARAUCO',
			'ARIC' => 'ARICA',
			'BUIN' => 'BUIN',
			'BULN' => 'BULNES',
			'CABR' => 'CABRERO',
			'CALA' => 'CALAMA',
			'CALB' => 'CALBUCO',
			'CALD' => 'CALDERA',
			'CANE' => 'CANETE',
			'CARA' => 'CARAHUE',
			'CART' => 'CARTAGENA',
			'CASA' => 'CASABLANCA',
			'CAST' => 'CASTRO',
			'CAUQ' => 'CAUQUENES',
			'CCHA' => 'CONCHALI',
			'CCON' => 'CONCON',
			'CHAN' => 'CHANARAL',
			'CHCH' => 'CHILE CHICO',
			'CHEP' => 'CHEPICA',
			'CHIG' => 'CHIGUAYANTE',
			'CHIL' => 'CHILLAN',
			'CHIM' => 'CHIMBARONGO',
			'CHON' => 'CHONCHI',
			'COCH' => 'COCHRANE',
			'COEL' => 'COELEMU',
			'COLB' => 'COLBUN',
			'COLI' => 'COLINA',
			'COLL' => 'COLLIPULLI',
			'COMB' => 'COMBARBALA',
			'CONC' => 'CONCEPCION',
			'CONS' => 'CONSTITUCION',
			'COPI' => 'COPIAPO',
			'COQU' => 'COQUIMBO',
			'CORO' => 'CORONEL',
			'COYH' => 'COYHAIQUE',
			'CRCV' => 'CURACAVI',
			'CURA' => 'CURANILAHUE',
			'CURC' => 'CURACAUTIN',
			'CURI' => 'CURICO',
			'DALC' => 'DALCAHUE',
			'DIEG' => 'DIEGO DE ALMAGRO',
			'DONI' => 'DONIHUE',
			'ECAR' => 'EL CARMEN',
			'ECEN' => 'ESTACION CENTRAL',
			'ELBO' => 'EL BOSQUE',
			'ENEA' => 'ENEA EXPRESS',
			'FRUT' => 'FRUTILLAR',
			'FUTR' => 'FUTRONO',
			'GRAN' => 'GRANEROS',
			'HAQI' => 'HUALQUI',
			'HORP' => 'HORNOPIREN',
			'HPEN' => 'HUALPEN',
			'HUAS' => 'HUASCO',
			'HUEC' => 'HUECHURABA',
			'ILLA' => 'ILLAPEL',
			'INDE' => 'INDEPENDENCIA',
			'INTE' => 'INTERNACIONAL',
			'IQUI' => 'IQUIQUE',
			'LACA' => 'LA CALERA',
			'LACI' => 'LA CISTERNA',
			'LACR' => 'LA CRUZ',
			'LAFL' => 'LA FLORIDA',
			'LAJA' => 'LAJA',
			'LALI' => 'LA LIGUA',
			'LAMP' => 'LAMPA',
			'LANC' => 'LANCO',
			'LAND' => 'LOS ANDES',
			'LANG' => 'LOS ANGELES',
			'LARE' => 'LA REINA',
			'LASE' => 'LA SERENA',
			'LAUN' => 'LA UNION',
			'LAUT' => 'LAUTARO',
			'LCAB' => 'LAS CABRAS',
			'LCHE' => 'LITUECHE',
			'LCON' => 'LAS CONDES',
			'LEBU' => 'LEBU',
			'LIMA' => 'LIMACHE',
			'LINA' => 'LINARES',
			'LLAG' => 'LOS LAGOS',
			'LLAN' => 'LLANQUIHUE',
			'LLAY' => 'LLAY LLAY',
			'LMUE' => 'LOS MUERMOS',
			'LOBA' => 'LO BARNECHEA',
			'LOES' => 'LO ESPEJO',
			'LOLO' => 'LOLOL',
			'LONC' => 'LONCOCHE',
			'LONG' => 'LONGAVI',
			'LOPR' => 'LO PRADO',
			'LOSC' => 'CERRILLOS',
			'LOTA' => 'LOTA',
			'LVIL' => 'LOS VILOS',
			'MACH' => 'MACHALI',
			'MACU' => 'MACUL',
			'MARC' => 'MARCHIGUE',
			'MARI' => 'MARIA ELENA',
			'MAUL' => 'MAULLIN',
			'MEJI' => 'MEJILLONES',
			'MELI' => 'MELIPILLA',
			'MIPU' => 'MAIPU',
			'MOLI' => 'MOLINA',
			'MOPA' => 'MONTE PATRIA',
			'MULC' => 'MULCHEN',
			'NACI' => 'NACIMIENTO',
			'NANC' => 'NANCAGUA',
			'NEGR' => 'NEGRETE',
			'NUNO' => 'NUNOA',
			'NVAI' => 'NUEVA IMPERIAL',
			'OLMU' => 'OLMUE',
			'OSOR' => 'OSORNO',
			'OVAL' => 'OVALLE',
			'PAIL' => 'PAILLACO',
			'PALT' => 'PUENTE ALTO',
			'PANG' => 'PANGUIPULLI',
			'PARR' => 'PARRAL',
			'PAYS' => 'PUERTO AYSEN',
			'PCIS' => 'PUERTO CISNES',
			'PEDR' => 'PEDRO AGUIRRE CERDA',
			'PENC' => 'PENCO',
			'PERA' => 'PERALILLO',
			'PEUM' => 'PEUMO',
			'PICD' => 'PICHIDEGUA',
			'PICH' => 'PICHILEMU',
			'PLCA' => 'PADRE LAS CASAS',
			'PLOL' => 'PENALOLEN',
			'PMON' => 'PUERTO MONTT',
			'PNAT' => 'NATALES',
			'PORV' => 'PORVENIR',
			'POZO' => 'POZO ALMONTE',
			'PROV' => 'PROVIDENCIA',
			'PUCH' => 'PUCHUNCAVI',
			'PUCO' => 'PUCON',
			'PUDA' => 'PUDAHUEL',
			'PUNI' => 'PUNITAQUI',
			'PUNT' => 'PUNTA ARENAS',
			'PURE' => 'PUREN',
			'PURR' => 'PURRANQUE',
			'PUYG' => 'PUYEHUE',
			'PVAR' => 'PUERTO VARAS',
			'QILI' => 'QUILICURA',
			'QLTA' => 'QUILLOTA',
			'QNOR' => 'QUINTA NORMAL',
			'QSCO' => 'EL QUISCO',
			'QUEL' => 'QUELLON',
			'QUEM' => 'QUEMCHI',
			'QUIL' => 'QUILPUE',
			'QUIN' => 'QUINTERO',
			'QUIR' => 'QUIRIHUE',
			'QULL' => 'QUILLON',
			'RANC' => 'RANCAGUA',
			'RECO' => 'RECOLETA',
			'RENC' => 'RENCA',
			'RENG' => 'RENGO',
			'REQU' => 'REQUINOA',
			'RIOB' => 'RIO BUENO',
			'RNEG' => 'RIO NEGRO',
			'SALA' => 'SALAMANCA',
			'SANT' => 'SAN ANTONIO',
			'SBAR' => 'SANTA BARBARA',
			'SBER' => 'SAN BERNARDO',
			'SCAR' => 'SAN CARLOS',
			'SCRU' => 'SANTA CRUZ',
			'SFEL' => 'SAN FELIPE',
			'SFER' => 'SAN FERNANDO',
			'SGOR' => 'SIERRA GORDA',
			'SJAV' => 'SAN JAVIER',
			'SJOA' => 'SAN JOAQUIN',
			'SMAR' => 'SANTA MARIA',
			'SMIG' => 'SAN MIGUEL',
			'SPAT' => 'SAN PEDRO DE ATACAMA',
			'SPED' => 'SAN PEDRO DE LA PAZ',
			'STGO' => 'SANTIAGO',
			'SVIC' => 'SAN VICENTE DE TAGUA TAGUA',
			'TALA' => 'TALAGANTE',
			'TALC' => 'TALCA',
			'TALT' => 'TALTAL',
			'TEMU' => 'TEMUCO',
			'TENO' => 'TENO',
			'THNO' => 'TALCAHUANO',
			'TOCO' => 'TOCOPILLA',
			'TOME' => 'TOME',
			'TRAI' => 'TRAIGUEN',
			'TUCA' => 'TUCAPEL',
			'VALD' => 'VALDIVIA',
			'VALE' => 'VILLA ALEMANA',
			'VALG' => 'VILLA ALEGRE',
			'VALL' => 'VALLENAR',
			'VALP' => 'VALPARAISO',
			'VICT' => 'VICTORIA',
			'VICU' => 'VICUNA',
			'VILL' => 'VILLARRICA',
			'VINA' => 'VINA DEL MAR',
			'VITA' => 'VITACURA',
			'YUMB' => 'YUMBEL',
			'YUNG' => 'YUNGAY',
			];
        $client = new \GuzzleHttp\Client();
        $headers = ['Content-Type' => 'application/json','Ocp-Apim-Subscription-Key' => $api_key];
        $body = array(
            'originCountyCode' => array_search($from,$comuna),
            'destinationCountyCode' => array_search($destination,$comuna),
            'package' => array(
                'weight' => $weight,
                'height' => $height,
                'width' => $width,
                'length' => $lenght,
            ),
            'productType' => 3,
            'contentType' => 1,
            'declaredWorth' => $declarated,
            'deliveryTime' => 0
        );
        $response = $client->post('https://testservices.wschilexpress.com/rating/api/v1.0/rates/courier',[
            'headers' => $headers,
            'body' => json_encode($body)
        ]);
        return array(
            "service_name"=> 'chilexpress',//gettype(json_decode($response->getBody(),true)['data']['courierServiceOptions'][0]['serviceDescription']),
            "service_code" => "ON",
            "total_price" => json_decode($response->getBody(),true)['data']['courierServiceOptions'][0]['serviceValue']."00",
            "currency" => "CLP",
            "description" => json_decode($response->getBody(),true)['data']['courierServiceOptions'][0]['serviceDescription'].' '.$from.'-'.$destination,
            "min_delivery_date" => "",
            "max_delivery_date" => ""
        );
	}
	public function RocketCurrier(){

	}
	public function OrderCreate(Request $request){
		//$tienda = $request->header('X-Shopify-Shop-Domain');
        /*$tienda = Shop::where('shopify_domain','=',$tienda)->first();
        $tienda->delete();*/
        $host = $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['SCRIPT_URI'];
        $mail_to='matulixxd@gmail.com';
        $mail_subject="Test email from $host";
        $mail_body='era get la wea mati qlo';
        $header = "Content-type: text/html\n";
        $header .= "From: \"PHP mail() Test Script\"<noreply@$host>\n";
        if(mail($mail_to, $mail_subject, $mail_body,$header))
        {
            return "wena chuchchetumare";
        }
        else
        {
            return "mala chuchchetumare";
        }
	}
}
