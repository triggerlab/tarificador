<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use ShopifyApp;
use App\User;
use App\currier;
use Illuminate\Support\Facades\Hash;
use App\Tienda;
use OhMyBrew\ShopifyApp\Traits\BillingControllerTrait;

class ShopifyController extends Controller
{    
    use BillingControllerTrait;
    /*
    $shop
    "id":1,"
    shopify_domain":"triggerlab.myshopify.com",
    "shopify_token":"389cd5c70234dda90e33b4026fa2d22f",
    "created_at":"2020-03-05 17:27:46",
    "updated_at":"2020-03-05 17:29:31",
    "grandfathered":false,
    "deleted_at":null,
    "namespace":null,
    "plan_id":null,
    "freemium":false}
    */
public function index(Request $request){
    $shop = ShopifyApp::shop();
    /*$body = array(
        "webhook" => [
            'topic' => env('SHOPIFY_WEBHOOK_1_TOPIC', 'app/uninstalled'),
            'address' => env('SHOPIFY_WEBHOOK_1_ADDRESS', 'https://triggerlab.cl/shopifylaravel/public/app-uninstalled'),
            'format' => 'json'
        ]
        );*/
        $this->afterInstall($shop);
        //$request = $shop->api()->rest('POST', '/admin/api/2020-01/webhooks.json',$body);
        //$body = collect($request);
        //$coll = new Collection($body);
        return Redirect('shop');
        //return $shop->shopify_domain;
        //return redirect('https://'.$shop->shopify_domain.'/admin/apps/triggerlab-tarificador');
}
public function afterInstall($shop){
        $request = $shop->api()->rest('GET', '/admin/api/2020-01/carrier_services.json');
        $body = collect($request->body->carrier_services);
        $coll = new Collection($body);
        if($coll->where('name','=','TriggerlabTar')->count() == 0){
            $body = array(
                "carrier_service" => [
                    "name" => "TriggerlabTar",
                    "callback_url" => "https://triggerlab.cl/tarificador/public/response",
                    "service_discovery"=> true
                ]
                );
            $shop->api()->rest('POST','/admin/api/2020-01/carrier_services.json',$body);
            
        }
        if(Tienda::porNombreTienda($shop->shopify_domain) == null){
            Tienda::create([
                'nombre_tienda' => $shop->shopify_domain
            ]);
            currier::create([
                'nombre' => 'Chilexpress',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
            currier::create([
                'nombre' => 'RocketCurrier',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
            currier::create([
                'nombre' => 'DHL',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
            currier::create([
                'nombre' => 'FedEx',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
            currier::create([
                'nombre' => 'Correos de Chile',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
            currier::create([
                'nombre' => 'Bluexpress',
                'api_key' => '',
                'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
            ]);
        }
        
        //return Tienda::porNombreTienda($shop->shopify_domain)->count();
    }
}
