<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnvioGratis extends Model
{
    protected $table ='envio_gratis';

    protected $primaryKey = 'id_envio';

    protected $fillable = ['condicion','valor','id_tarifa'];
}
