<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class currier extends Model
{
    protected $table = 'currier';

    protected $primaryKey = 'id_currier';

    protected $fillable = ['nombre','api_key','id_tienda'];
}
