<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tienda extends Model
{
    protected $table = 'tienda';

    protected $primaryKey = 'id_tienda';
    
    protected $fillable = ['nombre_tienda'];

    public static function porNombreTienda($tienda){
        return DB::table('tienda')
        ->select('*')
        ->where('nombre_tienda','=',$tienda)
        ->first();
    }
}
