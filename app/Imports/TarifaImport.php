<?php

namespace App\Imports;

use App\Tienda;
use App\Tarifa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Http\Request;
use ShopifyApp;

class TarifaImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $shop = ShopifyApp::shop();
        return Tarifa::firstOrCreate([
            'nombre_tarifa' => $row['nombre_tarifa'],
            'pais' => $row['pais'],
            'region' => $row['region'],
            'comuna' => $row['comuna'],
            'moneda' => $row['moneda'],
            'precio_base' => $row['precio_base'],
            'precio_x_kg' => $row['precio_x_kg'],
            'tiempo_min' => $row['tiempo_min'],
            'tiempo_max' => $row['tiempo_max'],
            'desde' => $row['desde'],
            'id_tienda' => Tienda::porNombreTienda($shop->shopify_domain)->id_tienda
        ]);
    }
}
